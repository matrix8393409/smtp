package main

import (
	"bytes"
	"fmt"
	"net/smtp"
	"text/template"
)

func sendMailSimple(subject string, body string, to []string) {
	auth := smtp.PlainAuth(
		"",
		"karimjonov6006@gmail.com",
		"fhnlkskngcmeovzm",
		"smtp.gmail.com",
	)
	header := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";"
	msg := "Subject: " + subject + "\n" + header + "\n\ns" + body

	err := smtp.SendMail(
		"smtp.gmail.com:587",
		auth,
		"karimjonov6006@gmail.com",
		to,
		[]byte(msg),
	)
	if err != nil {
		fmt.Println(err)
	}
}

func sendMailSimpleHTML(subject string, templatePath string, to []string) {
	// get html
	var body bytes.Buffer
	t, err := template.ParseFiles(templatePath)
	if err != nil {
		fmt.Println(err)
	}
	t.Execute(&body, struct{ Name string }{Name: "Shohruh"})
	auth := smtp.PlainAuth(
		"",
		"karimjonov6006@gmail.com",
		"fhnlkskngcmeovzm",
		"smtp.gmail.com",
	)

	header := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";"

	msg := "Subject: " + subject + "\n" + header + "\n\ns" + body.String()

	err = smtp.SendMail(
		"smtp.gmail.com:587",
		auth,
		"karimjonov6006@gmail.com",
		to,
		[]byte(msg),
	)
	if err != nil {
		fmt.Println(err)
	}
}

func main() {

	sendMailSimple("Результаты Лаборатории",
		`<!DOCTYPE html>
		<html lang="en">
		<head>
		  <meta charset="UTF-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <title>Document</title>
		</head>
		<body>
		  <h1>Пациент: Зокиров Бобур</h1>
		  <a href="https://cdn.medion.uz/docs/%D0%AD%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BB%D0%B8%zD1%82%D1%8B%20%D0%B8%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D1%8D%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82%D1%8B_2023-09-06_09:01.pdf">link1</a>
		  <div style="border: 1px solid red">
	
			<!DOCTYPE html>
		<html lang="en">
		<head>
		  <meta charset="UTF-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1.0">
		  <title>Document</title>
		</head>
		<body>
		  <h1>Пациент: Зокиров Бобур</h1>
		  <a href="https://cdn.medion.uz/docs/%D0%AD%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BB%D0%B8%zD1%82%D1%8B%20%D0%B8%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D1%8D%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82%D1%8B_2023-09-06_09:01.pdf">link1</a>
		  <embed
		src="https://cdn.medion.uz/docs/%D0%AD%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BB%D0%B8%D1%82%D1%8B%20%D0%B8%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D1%8D%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82%D1%8B_2023-09-06_09:01.pdf"
		type="application/pdf"
		frameBorder="0"
		scrolling="auto"
		height="100%"
		width="100%"
	></embed>
	
	<iframe
		src="https://cdn.medion.uz/docs/%D0%AD%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BB%D0%B8%D1%82%D1%8B%20%D0%B8%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D1%8D%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82%D1%8B_2023-09-06_09:01.pdf"
		frameBorder="0"
		scrolling="auto"
		height="100%"
		width="100%"
	></iframe>
		  
	<i class="fa fa-file-pdf-o" style="font-size:48px;color:red"></i>
	
		  <a href="https://cdn.medion.uz/docs/%D0%AD%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%BB%D0%B8%D1%82%D1%8B%20%D0%B8%20%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D1%8D%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82%D1%8B_2023-09-06_09:01.pdf">link2</a>
		</body>
		</html>`, []string{"medion_inn@analizy.uz"})

	// sendMailSimpleHTML("Another b=subject", "./test.html", []string{"thesamandarfoziljonov@gmail.com"})

}
